import React, {FC, useState} from 'react';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import {Button, Dialog, TableCell, TableRow, Typography} from '@material-ui/core';
import {actors} from '../../data/data';
import {Link, useParams} from 'react-router-dom';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import Paper from '@material-ui/core/Paper';

type Props = {};

const useClasses = makeStyles({
    root: {
        display: 'flex',
        height: '100%',
        padding: 10
    },
    imageBox: {
        margin: '32px 0 0',
        maxWidth: 600,
        flexGrow: 1,
        '& img': {
            width: '100%',
            objectFit: 'contain'
        }
    },
    contentBox: {
        flexGrow: 1,
    },
    contentField: {
        display: 'flex',
        alignItems: 'center'
    },
    fieldWrapper: {
        display: 'flex',
        alignItems: 'baseline',
        '& label': {
            width: 120
        }
    },
    cursorPointer: {
        cursor: 'pointer'
    },
    primaryButton: {
        color: 'white',
        backgroundColor: '#5aac44',
        '&:hover': {
            backgroundColor: 'black'
        }
    },
    secondaryButton: {
        color: 'white',
        backgroundColor: '#4e4e4c',
        '&:hover': {
            backgroundColor: 'black'
        }
    },
    fullName: {
        color: '#fdfdfd',
        cursor: 'default',
    }
})

const ActorPage: FC<Props> = () => {

    const useStyles = makeStyles({
        table: {
            minWidth: 150,
            backgroundColor: '#444444',
        },
    });

    const classes = useStyles();

    const {root, imageBox, contentBox, primaryButton, secondaryButton, fullName} = useClasses();
    const {id} = useParams();

    const actor = actors.find((item: any) => {
        return +item.id === +id
    })

    type Actor = ReturnType<typeof actor | any>;

    const {
        src, lastName, firstName, age, country, city, bodyConstitution, chest, chin, ears, eyeColor, eyeShape, faceShape, forehead,
        hairColor, hairLength, hasBeard, hasMustache, height, hips, lips, noseType, readyToRelocate, sex, videoLink,
        skills, waist, weight
    }: Actor = actor;

    const [open, setOpen] = useState(false);
    const [openDialog, setOpenDialog] = useState(false);

    const handleClickOpen = () => {
        setOpenDialog(true);
    };

    const handleClickClose = () => {
        setOpenDialog(false);
    };

    const StyledTableCell = withStyles((theme) => ({
        head: {
            backgroundColor: 'black',
            color: theme.palette.common.white,
        },
        body: {
            backgroundColor: '#878683',
            color: 'black',
            fontSize: 14,
        },
    }))(TableCell);

    const StyledTableRow = withStyles((theme) => ({
        head: {
            backgroundColor: theme.palette.common.black,
            color: theme.palette.common.white,
        },
        body: {
            fontSize: 14,
        },
    }))(TableRow);

    return <div>
        <div style={{width: 'fit-content'}}>
            <Button className={secondaryButton} component={Link} to="/" variant="contained" color="default">
                Назад
            </Button>
            <Button className={primaryButton} variant="contained" color="primary" onClick={handleClickOpen} style={{margin: '0 16px'}}>
                Запросити
            </Button>
        </div>
        <div className={root}>
            <div className={imageBox}>
                <img src={src} alt="" style={{marginBottom: 25, boxShadow: '0px 0px 15px 5px rgba(0, 0, 0, .2)'}}/>

                <div style={{width: '100%'}}>
                    <iframe style={{width: '100%'}} height="300" src={videoLink} frameBorder="0"
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                            allowFullScreen/>
                </div>
            </div>
            <Dialog onClose={handleClickClose} aria-labelledby="simple-dialog-title" open={openDialog}>
                <div style={{padding: 32, display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
                    <Typography variant="body2" style={{margin: '15px'}}>
                        Телефон: +380 63 119 9945
                    </Typography>
                    <Button variant="contained" className={primaryButton}>
                        <a href="tel:+380 63 119 9945" style={{textDecoration: 'none', color: 'white'}}>
                            Зателефонувати
                        </a>
                    </Button>
                </div>
            </Dialog>
            <div className={contentBox}>
                <div>
                    <Typography className={fullName} variant="h5" style={{padding: 30}}>
                        {`${firstName} ${lastName}`}
                    </Typography>
                    <div style={{padding: '0 32px'}}>
                        <TableContainer component={Paper}>
                            <Table className={classes.table}>
                                <TableHead>
                                    <StyledTableRow>
                                        <StyledTableCell>Параметр</StyledTableCell>
                                        <StyledTableCell>Опис</StyledTableCell>
                                    </StyledTableRow>
                                </TableHead>
                                <TableBody>
                                    <StyledTableRow>
                                        <StyledTableCell>Вік</StyledTableCell>
                                        <StyledTableCell>{age}</StyledTableCell>
                                    </StyledTableRow>
                                    <StyledTableRow>
                                        <StyledTableCell>Стать</StyledTableCell>
                                        <StyledTableCell>{sex}</StyledTableCell>
                                    </StyledTableRow>
                                    <StyledTableRow>
                                        <StyledTableCell>Країна</StyledTableCell>
                                        <StyledTableCell>{country}</StyledTableCell>
                                    </StyledTableRow>
                                    <StyledTableRow>
                                        <StyledTableCell>Місто</StyledTableCell>
                                        <StyledTableCell>{city}</StyledTableCell>
                                    </StyledTableRow>
                                    <StyledTableRow>
                                        <StyledTableCell>Готовність переїхати</StyledTableCell>
                                        <StyledTableCell>{readyToRelocate}</StyledTableCell>
                                    </StyledTableRow>
                                    <StyledTableRow>
                                        <StyledTableCell>Статура</StyledTableCell>
                                        <StyledTableCell>{bodyConstitution}</StyledTableCell>
                                    </StyledTableRow>
                                    <StyledTableRow>
                                        <StyledTableCell>Зріст (см)</StyledTableCell>
                                        <StyledTableCell>{height}</StyledTableCell>
                                    </StyledTableRow>
                                    <StyledTableRow>
                                        <StyledTableCell>Вага (кг)</StyledTableCell>
                                        <StyledTableCell>{weight}</StyledTableCell>
                                    </StyledTableRow>
                                    <StyledTableRow>
                                        <StyledTableCell>Груди (см)</StyledTableCell>
                                        <StyledTableCell>{chest}</StyledTableCell>
                                    </StyledTableRow>
                                    <StyledTableRow>
                                        <StyledTableCell>Талія (см)</StyledTableCell>
                                        <StyledTableCell>{waist}</StyledTableCell>
                                    </StyledTableRow>
                                    <StyledTableRow>
                                        <StyledTableCell>Стегна (см)</StyledTableCell>
                                        <StyledTableCell>{hips}</StyledTableCell>
                                    </StyledTableRow>
                                    <StyledTableRow>
                                        <StyledTableCell>Колір волосся</StyledTableCell>
                                        <StyledTableCell>{hairColor}</StyledTableCell>
                                    </StyledTableRow>
                                    <StyledTableRow>
                                        <StyledTableCell>Довжина волосся</StyledTableCell>
                                        <StyledTableCell>{hairLength}</StyledTableCell>
                                    </StyledTableRow>
                                    <StyledTableRow>
                                        <StyledTableCell>Форма обличчя</StyledTableCell>
                                        <StyledTableCell>{faceShape}</StyledTableCell>
                                    </StyledTableRow>
                                    <StyledTableRow>
                                        <StyledTableCell>Лоб</StyledTableCell>
                                        <StyledTableCell>{forehead}</StyledTableCell>
                                    </StyledTableRow>
                                    <StyledTableRow>
                                        <StyledTableCell>Форма очей</StyledTableCell>
                                        <StyledTableCell>{eyeShape}</StyledTableCell>
                                    </StyledTableRow>
                                    <StyledTableRow>
                                        <StyledTableCell>Колір очей</StyledTableCell>
                                        <StyledTableCell>{eyeColor}</StyledTableCell>
                                    </StyledTableRow>
                                    <StyledTableRow>
                                        <StyledTableCell>Форма носу</StyledTableCell>
                                        <StyledTableCell>{noseType}</StyledTableCell>
                                    </StyledTableRow>
                                    <StyledTableRow>
                                        <StyledTableCell>Вуха</StyledTableCell>
                                        <StyledTableCell>{ears}</StyledTableCell>
                                    </StyledTableRow>
                                    <StyledTableRow>
                                        <StyledTableCell>Губи</StyledTableCell>
                                        <StyledTableCell>{lips}</StyledTableCell>
                                    </StyledTableRow>
                                    <StyledTableRow>
                                        <StyledTableCell>Підборіддя</StyledTableCell>
                                        <StyledTableCell>{chin}</StyledTableCell>
                                    </StyledTableRow>
                                    <StyledTableRow>
                                        <StyledTableCell>Наявність вус</StyledTableCell>
                                        <StyledTableCell>{hasMustache}</StyledTableCell>
                                    </StyledTableRow>
                                    <StyledTableRow>
                                        <StyledTableCell>Наявність бороди</StyledTableCell>
                                        <StyledTableCell>{hasBeard}</StyledTableCell>
                                    </StyledTableRow>
                                    <StyledTableRow>
                                        <StyledTableCell>Навички та вміння</StyledTableCell>
                                        <StyledTableCell>{skills.join(", ")}</StyledTableCell>
                                    </StyledTableRow>
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </div>
                </div>
            </div>
        </div>
    </div>
};

export default ActorPage;
