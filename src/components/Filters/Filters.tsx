import React, {FC} from 'react';
import {useFormik} from 'formik';
import {
    Button,
    Checkbox,
    createStyles,
    FormControl,
    FormControlLabel,
    InputLabel,
    MenuItem,
    Select,
    Slider,
    TextField,
    Theme,
    Typography
} from '@material-ui/core';
import * as Yup from 'yup';
import {actors} from '../../data/data';
import {actorsDataSuccess} from '../../slices/actors';
import {useDispatch} from 'react-redux';
import {Autocomplete} from '@material-ui/lab';
import {makeStyles} from "@material-ui/core/styles";
import Divider from '@material-ui/core/Divider';

type Props = {};

const initialValues = {
    age: [3, 100],
    sex: '',
    city: '',
    readyToRelocate: false,
    bodyConstitution: '',
    height: [60, 200],
    weight: [30, 150],
    chest: [40, 150],
    waist: [40, 140],
    hips: [40, 150],
    country: '',
    hairColor: '',
    hairLength: '',
    faceShape: '',
    forehead: '',
    eyeShape: '',
    eyeColor: '',
    noseType: '',
    ears: '',
    lips: '',
    chin: '',
    hasMustache: false,
    hasBeard: false,
    skills: []
};

const initialSkills = [
    {value: 'Вокал'},
    {value: 'Музичні інструменти'},
    {value: 'Хореографія'},
    {value: 'Водіння авто'},
    {value: 'Сценічний бій'},
    {value: 'Володіння зброєю'},
    {value: 'Знання іноземних мов'}
]

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        text: {
            padding: theme.spacing(2, 2, 0),
        },
        paper: {
            paddingBottom: 50,
            backgroundColor: '#878683',
        },
        list: {
            marginBottom: theme.spacing(2),
        },
        subheader: {
            backgroundColor: theme.palette.background.paper,
        },
        appBar: {
            top: 'auto',
            bottom: 0,
        },
        primaryButton: {
            color: 'white',
            backgroundColor: '#4e4e4c',
            '&:hover': {
                backgroundColor: 'black'
            }
        },
        slider: {
            color: 'black',
        },
        checkbox: {
            color: 'black',
        },
        select: {
            '&.MuiInput-underline:after': {
                borderBottomColor: 'black',
            },
            '.mui-input-label.focused': {
                color: "red",
            },
        },
    }),
);

const Filters: FC<Props> = () => {

    const classes = useStyles();

    function valuetext(value: number) {
        return `${value}`;
    }

    const dispatch = useDispatch()

    const valueBetween = (values: any, value: any) => {
        return value >= values[0] && value <= values[1];
    }

    const checkBooleanValue = (inputValue: string) =>
        inputValue === 'Так';

    const matchAutocomplete = (actorSkills: any[], skills: string[]) => {
        const result = actorSkills.map((actorSkill: any) => {
            return skills.find((skill: string) => {
                return actorSkill.value === skill
            });
        }).filter(Boolean);

        return result.length === actorSkills.length;
    }

    const filter = (val: any) => {
        const filteredActors = []

        for (const actor of actors) {
            const {
                age, country, city, bodyConstitution, chest, chin, ears, eyeColor,
                eyeShape, faceShape, forehead, hairColor, hairLength, hasBeard,
                hasMustache, height, hips, lips, noseType, readyToRelocate,
                sex, skills, waist, weight
            } = actor;

            if (val.age !== "" && !valueBetween(val.age, age)) continue;
            if (val.sex !== "" && val.sex !== sex) continue;
            if (val.country !== "" && val.country !== country) continue;
            if (val.city !== "" && val.city !== city) continue;
            if (val.bodyConstitution !== "" && val.bodyConstitution !== bodyConstitution) continue;
            if (val.height !== "" && !valueBetween(val.height, height)) continue;
            if (val.weight !== "" && !valueBetween(val.weight, weight)) continue;
            if (val.chest !== "" && !valueBetween(val.chest, chest)) continue;
            if (val.waist !== "" && !valueBetween(val.waist, waist)) continue;
            if (val.hips !== "" && !valueBetween(val.hips, hips)) continue;
            if (val.hairLength !== "" && val.hairLength !== hairLength) continue;
            if (val.hairColor !== "" && val.hairColor !== hairColor) continue;
            if (val.faceShape !== "" && val.faceShape !== faceShape) continue;
            if (val.forehead !== "" && val.forehead !== forehead) continue;
            if (val.eyeShape !== "" && val.eyeShape !== eyeShape) continue;
            if (val.eyeColor !== "" && val.eyeColor !== eyeColor) continue;
            if (val.noseType !== "" && val.noseType !== noseType) continue;
            if (val.ears !== "" && val.ears !== ears) continue;
            if (val.lips !== "" && val.lips !== lips) continue;
            if (val.chin !== "" && val.chin !== chin) continue;
            if (val.skills !== "" && !matchAutocomplete(val.skills, skills)) continue;
            if (val.readyToRelocate && !checkBooleanValue(readyToRelocate)) continue;
            if (val.hasMustache && !checkBooleanValue(hasMustache)) continue;
            if (val.hasBeard && !checkBooleanValue(hasBeard)) continue;

            filteredActors.push(actor);
        }

        dispatch(actorsDataSuccess(filteredActors));
    }

    const {values, handleChange, handleSubmit, setFieldValue} = useFormik({
        initialValues,
        validationSchema: Yup.object().shape({}),
        onSubmit: filter
    })

    const handleAutocomplete = (val: any, newSkills: any) => {
        setFieldValue('skills', newSkills);
    }

    const handleSliderChange = (name: any) => (event: any, value: any) => {
        setFieldValue(name, value);
    };

    return (
        <form onSubmit={handleSubmit} style={{width: 320, display: 'flex', flexDirection: 'column', padding: 16}}>
            <FormControl>
                <Typography id="range-slider" color="textSecondary" gutterBottom variant="caption"
                            style={{textAlign: 'left'}}>
                    Вік
                </Typography>
                <Slider
                    className={classes.slider}
                    name="age"
                    value={values.age}
                    onChange={handleSliderChange('age')}
                    valueLabelDisplay="auto"
                    aria-labelledby="range-slider"
                    getAriaValueText={valuetext}
                    min={3}
                    max={100}
                    marks={[{value: 3, label: 3}, {value: 100, label: 100}]}
                />
            </FormControl>
            <FormControl>
                <InputLabel id="sex-select">Стать</InputLabel>
                <Select
                    className={classes.select}
                    labelId="sex-select" name="sex" value={values.sex} onChange={handleChange}>
                    <MenuItem value="" style={{height: 35}}/>
                    <MenuItem value="Чоловік">Чоловік</MenuItem>
                    <MenuItem value="Жінка">Жінка</MenuItem>
                </Select>
            </FormControl>

            <FormControl>
                <InputLabel id="country-select">
                    Країна
                </InputLabel>
                <Select
                    className={classes.select}
                    labelId="country-select" name="country" value={values.country} onChange={handleChange}>
                    <MenuItem value="" style={{height: 35}}/>
                    <MenuItem value="Україна">Україна</MenuItem>
                    <MenuItem value="Росія">Росія</MenuItem>
                </Select>
            </FormControl>
            <FormControl>
                <InputLabel id="city-select">
                    Місто
                </InputLabel>
                <Select
                    className={classes.select}
                    labelId="city-select" name="city" value={values.city} onChange={handleChange}>
                    <MenuItem value="" style={{height: 35}}/>
                    <MenuItem value="Київ">Київ</MenuItem>
                    <MenuItem value="Львів">Львів</MenuItem>
                    <MenuItem value="Одеса">Одеса</MenuItem>
                    <MenuItem value="Харків">Харків</MenuItem>
                    <MenuItem value="Черкаси">Черкаси</MenuItem>
                </Select>
            </FormControl>
            <FormControlLabel
                control={<Checkbox name="readyToRelocate" style={{color: 'black'}} checked={values.readyToRelocate}
                                   onChange={handleChange}
                />}
                label="Готовність переїхати"/>

            <FormControl>
                <InputLabel id="sex-select">Статура</InputLabel>
                <Select
                    className={classes.select}
                    labelId="sex-select" name="bodyConstitution" value={values.bodyConstitution}
                    onChange={handleChange}>
                    <MenuItem value="" style={{height: 35}}/>
                    <MenuItem value="Ектоморф">Ектоморф</MenuItem>
                    <MenuItem value="Мезоморф">Мезоморф</MenuItem>
                    <MenuItem value="Ендоморф">Ендоморф</MenuItem>
                </Select>
            </FormControl>

            <FormControl>
                <Typography id="range-slider" color="textSecondary" gutterBottom variant="caption"
                            style={{textAlign: 'left'}}>
                    Зріст (см)
                </Typography>
                <Slider
                    className={classes.slider}
                    name="height"
                    value={values.height}
                    onChange={handleSliderChange('height')}
                    valueLabelDisplay="auto"
                    aria-labelledby="range-slider"
                    getAriaValueText={valuetext}
                    max={200}
                    min={60}
                    marks={[{value: 60, label: 60}, {value: 200, label: 200}]}
                />
            </FormControl>

            <FormControl>
                <Typography id="range-slider" color="textSecondary" gutterBottom variant="caption"
                            style={{textAlign: 'left'}}>
                    Вага (кг)
                </Typography>
                <Slider
                    className={classes.slider}
                    name="weight"
                    value={values.weight}
                    onChange={handleSliderChange('weight')}
                    valueLabelDisplay="auto"
                    aria-labelledby="range-slider"
                    getAriaValueText={valuetext}
                    min={30}
                    max={150}
                    marks={[{value: 30, label: 30}, {value: 150, label: 150}]}
                />
            </FormControl>

            <FormControl>
                <Typography id="range-slider" color="textSecondary" gutterBottom variant="caption"
                            style={{textAlign: 'left'}}>
                    Груди (см)
                </Typography>
                <Slider
                    className={classes.slider}
                    name="chest"
                    value={values.chest}
                    onChange={handleSliderChange('chest')}
                    valueLabelDisplay="auto"
                    aria-labelledby="range-slider"
                    getAriaValueText={valuetext}
                    min={40}
                    max={150}
                    marks={[{value: 40, label: 40}, {value: 150, label: 150}]}
                />
            </FormControl>

            <FormControl>
                <Typography id="range-slider" color="textSecondary" gutterBottom variant="caption"
                            style={{textAlign: 'left'}}>
                    Талія (см)
                </Typography>
                <Slider
                    className={classes.slider}
                    name="waist"
                    value={values.waist}
                    onChange={handleSliderChange('waist')}
                    valueLabelDisplay="auto"
                    aria-labelledby="range-slider"
                    getAriaValueText={valuetext}
                    min={40}
                    max={140}
                    marks={[{value: 40, label: 40}, {value: 140, label: 140}]}
                />
            </FormControl>

            <FormControl>
                <Typography id="range-slider" color="textSecondary" gutterBottom variant="caption"
                            style={{textAlign: 'left'}}>
                    Стегна (см)
                </Typography>
                <Slider
                    className={classes.slider}
                    name="hips"
                    value={values.hips}
                    onChange={handleSliderChange('hips')}
                    valueLabelDisplay="auto"
                    aria-labelledby="range-slider"
                    getAriaValueText={valuetext}
                    min={40}
                    max={150}
                    marks={[{value: 40, label: 40}, {value: 150, label: 150}]}
                />
            </FormControl>

            <FormControl>
                <InputLabel id="hairColor-select">Колір волосся</InputLabel>
                <Select
                    className={classes.select}
                    labelId="hairColor-select" name="hairColor" value={values.hairColor} onChange={handleChange}>
                    <MenuItem value="" style={{height: 35}}/>
                    <MenuItem value="Русявий">Русявий</MenuItem>
                    <MenuItem value="Блонд">Блонд</MenuItem>
                    <MenuItem value="Чорний">Чорний</MenuItem>
                    <MenuItem value="Рудий">Рудий</MenuItem>
                    <MenuItem value="Каштановий">Каштановий</MenuItem>
                    <MenuItem value="Світло-каштановий">Світло-каштановий</MenuItem>
                    <MenuItem value="Темно-каштановий">Темно-каштановий</MenuItem>
                    <MenuItem value="Сивий">Сивий</MenuItem>
                    <MenuItem value="Неповторний">Неповторний</MenuItem>
                </Select>
            </FormControl>

            <FormControl>
                <InputLabel id="hairLength-select">Довжина волосся</InputLabel>
                <Select
                    className={classes.select}
                    labelId="hairLength-select" name="hairLength" value={values.hairLength} onChange={handleChange}>
                    <MenuItem value="" style={{height: 35}}/>
                    <MenuItem value="Коротке">Коротке</MenuItem>
                    <MenuItem value="Середня довжина">Середня довжина</MenuItem>
                    <MenuItem value="Чорний">Довге</MenuItem>
                </Select>
            </FormControl>

            <FormControl>
                <InputLabel id="faceShape-select">Форма обличчя</InputLabel>
                <Select
                    className={classes.select}
                    labelId="faceShape-select" name="faceShape" value={values.faceShape} onChange={handleChange}>
                    <MenuItem value="" style={{height: 35}}/>
                    <MenuItem value="Овальне">Овальне</MenuItem>
                    <MenuItem value="Видовжене">Видовжене</MenuItem>
                    <MenuItem value="Кругле">Кругле</MenuItem>
                    <MenuItem value="Квадратне">Квадратне</MenuItem>
                    <MenuItem value="Трикутне">Трикутне</MenuItem>
                    <MenuItem value="Грушевидне">Грушевидне</MenuItem>
                    <MenuItem value="Неповторне">Неповторне</MenuItem>
                </Select>
            </FormControl>

            <FormControl>
                <InputLabel id="forehead-select">Лоб</InputLabel>
                <Select
                    className={classes.select}
                    labelId="forehead-select" name="forehead" value={values.forehead} onChange={handleChange}>
                    <MenuItem value="" style={{height: 35}}/>
                    <MenuItem value="Високий і широкий">Високий і широкий</MenuItem>
                    <MenuItem value="Широкий і низький">Широкий і низький</MenuItem>
                    <MenuItem value="Високий і вузький"> Високий і вузький</MenuItem>
                    <MenuItem value="Прямий">Прямий</MenuItem>
                    <MenuItem value="З нахилом">З нахилом</MenuItem>
                </Select>
            </FormControl>

            <FormControl>
                <InputLabel id="eyeShape-select">Форма очей</InputLabel>
                <Select
                    className={classes.select}
                    labelId="eyeShape-select" name="eyeShape" value={values.eyeShape} onChange={handleChange}>
                    <MenuItem value="" style={{height: 35}}/>
                    <MenuItem value="Нависла повіка">Нависла повіка</MenuItem>
                    <MenuItem value="Мигдалеподібні">Мигдалеподібні</MenuItem>
                    <MenuItem value="Близько посаджені очі">Близько посаджені очі</MenuItem>
                    <MenuItem value="Глибоко посаджені очі">Глибоко посаджені очі</MenuItem>
                    <MenuItem value="З опущеними кутиками">З опущеними кутиками</MenuItem>
                    <MenuItem value="Розкосі">Розкосі</MenuItem>
                    <MenuItem value="Одне око">Одне око</MenuItem>
                </Select>
            </FormControl>

            <FormControl>
                <InputLabel id="eyeColor-select">Колір очей</InputLabel>
                <Select
                    className={classes.select}
                    labelId="eyeColor-select" name="eyeColor" value={values.eyeColor} onChange={handleChange}>
                    <MenuItem value="" style={{height: 35}}/>
                    <MenuItem value="Блакитний">Блакитний</MenuItem>
                    <MenuItem value="Сірий">Сірий</MenuItem>
                    <MenuItem value="Сіро-блакитний">Сіро-блакитний</MenuItem>
                    <MenuItem value="Зелений">Зелений</MenuItem>
                    <MenuItem value="Сіро-зелений">Сіро-зелений</MenuItem>
                    <MenuItem value="Темно-карий">Темно-карий</MenuItem>
                    <MenuItem value="Світло-карий">Світло-карий</MenuItem>
                </Select>
            </FormControl>

            <FormControl>
                <InputLabel id="noseType-select">Форма носу</InputLabel>
                <Select
                    className={classes.select}
                    labelId="noseType-select" name="noseType" value={values.noseType} onChange={handleChange}>
                    <MenuItem value="" style={{height: 35}}/>
                    <MenuItem value="Довгий ніс">Довгий ніс</MenuItem>
                    <MenuItem value="Ніс картоплею">Ніс картоплею</MenuItem>
                    <MenuItem value="Ніс з горбинкою">Ніс з горбинкою</MenuItem>
                    <MenuItem value="Короткий ніс">Короткий ніс</MenuItem>ss
                    <MenuItem value="Ніс з горбинкою">Ніс з горбинкою</MenuItem>
                    <MenuItem value="Орлиний ніс">Орлиний ніс</MenuItem>
                </Select>
            </FormControl>

            <FormControl>
                <InputLabel id="ears-select">Вуха</InputLabel>
                <Select
                    className={classes.select}
                    labelId="ears-select" name="ears" value={values.ears} onChange={handleChange}>
                    <MenuItem value="" style={{height: 35}}/>
                    <MenuItem value="Круглі">Круглі</MenuItem>
                    <MenuItem value="Вуглуваті">Вуглуваті</MenuItem>
                    <MenuItem value="Загострені">Загострені</MenuItem>
                    <MenuItem value="Стирчачі">Стирчачі</MenuItem>
                    <MenuItem value="Широка мочка">Широка мочка</MenuItem>
                    <MenuItem value="Прилягаюча мочка">Прилягаюча мочка</MenuItem>
                </Select>
            </FormControl>

            <FormControl>
                <InputLabel id="lips-select">Губи</InputLabel>
                <Select
                    className={classes.select}
                    labelId="lips-select" name="lips" value={values.lips} onChange={handleChange}>
                    <MenuItem value="" style={{height: 35}}/>
                    <MenuItem value="Сердечком">Сердечком</MenuItem>
                    <MenuItem value="Пухкі">Пухкі</MenuItem>
                    <MenuItem value="Вузькі">Вузькі</MenuItem>
                    <MenuItem value="Асиметричні">Асиметричні</MenuItem>
                    <MenuItem value="З припущеними кутиками">З припущеними кутиками</MenuItem>
                    <MenuItem value="З піднятими кутиками">З піднятими кутиками</MenuItem>
                </Select>
            </FormControl>

            <FormControl>
                <InputLabel id="chin-select">Підборіддя</InputLabel>
                <Select
                    className={classes.select}
                    labelId="chin-select" name="chin" value={values.chin} onChange={handleChange}>
                    <MenuItem value="" style={{height: 35}}/>
                    <MenuItem value="Овальне">Овальне</MenuItem>
                    <MenuItem value="Гостре">Гостре</MenuItem>
                    <MenuItem value="Скошене">Скошене</MenuItem>
                    <MenuItem value="Квадратне">Квадратне</MenuItem>
                    <MenuItem value="Виступаюче">Виступаюче</MenuItem>
                </Select>
            </FormControl>

            <FormControlLabel
                control={<Checkbox style={{color: 'black'}} name="hasMustache"
                                   value={undefined} /*checked={values.hasMustache}*/
                                   onChange={handleChange}
                />}
                label="Наявність вус"/>

            <FormControlLabel
                control={<Checkbox style={{color: 'black'}} name="hasBeard" checked={values.hasBeard}
                                   onChange={handleChange}
                />}
                label="Наявність бороди"/>


            <Autocomplete
                style={{margin: '0 0 16px 0'}}
                multiple
                id="tags-standard"
                options={initialSkills}
                getOptionLabel={(option) => option.value}
                onChange={handleAutocomplete}
                renderInput={(params) => (
                    <TextField
                        {...params}
                        variant="standard"
                        label="Навички та вміння"
                        placeholder="Навички та вміння"
                    />
                )}
            />

            <Divider/>
            <div style={{display: 'flex', justifyContent: 'center', paddingTop: 15}}>
                <Button type="submit" variant="contained" className={classes.primaryButton}>
                    Знайти актора
                </Button>
            </div>
        </form>
    );
};

export default Filters;
