import React, {FC} from 'react';
import {Button, Card, CardActions, CardContent, Typography} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';
import {Link} from 'react-router-dom';

type Props = {
    firstName: string;
    lastName: string;
    banner: string;
    description: string;
    src: string;
    id: number;
};

const useClasses = makeStyles({
    root: {
        width: 300,
        height: 380,
        padding: 0,
        boxShadow: '0px 0px 15px 5px rgba(0, 0, 0, .2)',
        backgroundColor: '#878683',
    },
    rootContent: {
        padding: 0,
    },
    image: {
        width: '100%',
        height: 280,
        objectFit: 'cover'
    },
    button: {
        color: 'white',
        backgroundColor: '#4e4e4c',
        '&:hover': {
            backgroundColor: 'black'
        }
    },
});

const ActorCard: FC<Props> = ({firstName, lastName, description, src, id}) => {
    const {root, rootContent, image, button} = useClasses();

    return (
        <Card className={root}>
            <CardContent className={rootContent}>
                <div>
                    <img className={image} src={src} alt="actor-banner-preview"/>
                </div>
                <Typography variant="h5" style={{textAlign: 'center', paddingTop: 8}}>
                    {`${firstName} ${lastName}`}
                </Typography>
                {description}
            </CardContent>
            <CardActions style={{justifyContent: 'flex-end'}}>
                <Button
                    className={button} to={`/actor/${id}`} component={Link} variant="contained" color="primary">
                    Перегляд
                </Button>
            </CardActions>
        </Card>
    );
}

export default ActorCard;
